package com.sopherwang.myapplication.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;

import com.sopherwang.myapplication.data.datasource.PagingRequestHelper;
import com.sopherwang.myapplication.data.models.ModelMessage;
import com.sopherwang.myapplication.data.repositories.MessageDataRepository;


/**
 * Created by Jiajun Wang on 1/11/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MainViewModel extends ViewModel {
    private MessageDataRepository mMessageDataRepository;

    public MainViewModel() {
        mMessageDataRepository = MessageDataRepository.getInstance();
    }

    LiveData<PagedList<ModelMessage>> getMessageFromDb() {
        return mMessageDataRepository.getMessageFromDb();
    }

    LiveData<PagingRequestHelper.StatusReport> getPagingStatus() {
        return mMessageDataRepository.getPagingStatus();
    }

    void deleteMessage(long id) {
        mMessageDataRepository.deleteMessage(id);
    }

    void retryOnError() {
        mMessageDataRepository.retryOnError();
    }
}
