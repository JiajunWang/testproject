package com.sopherwang.myapplication.ui.main;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sopherwang.myapplication.R;
import com.sopherwang.myapplication.data.models.ModelMessage;
import com.sopherwang.myapplication.data.network.retrofit.ApiStores;
import com.sopherwang.myapplication.utils.DataUtil;

/**
 * Created by Jiajun Wang on 1/14/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MessagePageAdapter extends PagedListAdapter<ModelMessage, MessagePageAdapter.ViewHolder> {
    private Context mContext;

    MessagePageAdapter(Context context) {
        super(DIFF_CALLBACK);
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_message, parent, false);
        return new MessagePageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelMessage modelMessage = getItem(position);
        Glide.with(mContext).load(ApiStores.BASE_URL + modelMessage.getAuthor().getPhotoUrl())
                .apply(RequestOptions.circleCropTransform()).into(holder.mAvatar);
        holder.mName.setText(modelMessage.getAuthor().getName());
        holder.mTime.setText(DataUtil.convertTimeString(modelMessage.getUpdate()));
        holder.mMessage.setText(modelMessage.getContent());
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mAvatar;
        private TextView mName;
        private TextView mTime;
        private TextView mMessage;

        ViewHolder(View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mTime = itemView.findViewById(R.id.time);
            mMessage = itemView.findViewById(R.id.message);
        }
    }

    private static final DiffCallback<ModelMessage> DIFF_CALLBACK = new DiffCallback<ModelMessage>() {
        @Override
        public boolean areItemsTheSame(@NonNull ModelMessage oldItem, @NonNull ModelMessage newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ModelMessage oldItem, @NonNull ModelMessage newItem) {
            return oldItem.getId() == newItem.getId();
        }
    };
}
