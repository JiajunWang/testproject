package com.sopherwang.myapplication.ui.main;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.sopherwang.myapplication.AppConfig;
import com.sopherwang.myapplication.R;
import com.sopherwang.myapplication.data.datasource.PagingRequestHelper;
import com.sopherwang.myapplication.data.models.ModelMessage;
import com.sopherwang.myapplication.ui.base.BaseActivity;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

/**
 * Created by Jiajun Wang on 1/11/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MainActivity extends BaseActivity {
    private static final String TAG = AppConfig.LOG_PREFIX + MainActivity.class.getSimpleName();
    private static final String KEY_LIST_POSITION = "KEY_LIST_POSITION";
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFloatingActionButton;

    private MainViewModel mMainViewModel;
    private LinearLayoutManager mLayoutManager;
    private MessagePageAdapter mMessagePageAdapter;
    private int mPreviousPosition = -1;
    private boolean mIsShowingFab = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        if (savedInstanceState != null) {
            mPreviousPosition = savedInstanceState.getInt(KEY_LIST_POSITION, -1);
        }
        initViews();
        getMessageFromDb();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_LIST_POSITION, mLayoutManager.findFirstVisibleItemPosition());
    }

    private void initViews() {
        setTitle(R.string.title_main_activity);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburg_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mFloatingActionButton = findViewById(R.id.fab);
        mFloatingActionButton.setOnClickListener(view -> retryOnError());
        initRecyclerViewAndAdapter();
    }

    private void initRecyclerViewAndAdapter() {
        mMessagePageAdapter = new MessagePageAdapter(this);
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mMessagePageAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            //Remove swiped item from list and notify the RecyclerView
            int position = viewHolder.getAdapterPosition();
            mMainViewModel.deleteMessage(mMessagePageAdapter.getCurrentList().get(position).getId());
        }
    };

    private void getMessageFromDb() {
        mMainViewModel.getMessageFromDb().observe(this, new Observer<PagedList<ModelMessage>>() {
            @Override
            public void onChanged(@Nullable PagedList<ModelMessage> modelMessages) {
                mMessagePageAdapter.setList(modelMessages);
                if (mPreviousPosition != -1 && mLayoutManager.findFirstVisibleItemPosition() != mPreviousPosition) {
                    mRecyclerView.scrollToPosition(mPreviousPosition);
                }
            }
        });
        mMainViewModel.getPagingStatus().observe(this, statusReport -> {
            if (statusReport != null) {
                if (statusReport.hasRunning()) {
                    //Loading
                    Log.d(TAG, "getPagingStatus() loading.");
                } else if (statusReport.hasError()) {
                    //Error
                    processingErrorsFromReport(statusReport);
                } else {
                    //Success
                    Log.d(TAG, "getPagingStatus() success.");
                    hideRetryFab();
                }
            }
        });
    }

    private void processingErrorsFromReport(PagingRequestHelper.StatusReport statusReport) {
        Throwable throwable;
        if (statusReport.initial == PagingRequestHelper.Status.FAILED) {
            Log.e(TAG, "processingErrorsFromReport() from Initial");
            throwable = statusReport.getErrorFor(PagingRequestHelper.RequestType.INITIAL);
        } else if (statusReport.before == PagingRequestHelper.Status.FAILED) {
            Log.e(TAG, "processingErrorsFromReport() from Before");
            throwable = statusReport.getErrorFor(PagingRequestHelper.RequestType.BEFORE);
        } else {
            Log.e(TAG, "processingErrorsFromReport() from After");
            throwable = statusReport.getErrorFor(PagingRequestHelper.RequestType.AFTER);
        }
        handleError(throwable);
    }

    private void handleError(Throwable error) {
        if (error instanceof SocketException || error instanceof UnknownHostException
                || error instanceof SocketTimeoutException || error instanceof SSLException) {
            //No internet
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
            showRetryFab();
        }
    }

    private void showRetryFab() {
        Log.d(TAG, "showRetryFab()");
        if (mFloatingActionButton != null && mFloatingActionButton.getVisibility() != View.VISIBLE) {
            mIsShowingFab = true;
            startFabAnimation();
        }
    }

    private void hideRetryFab() {
        Log.d(TAG, "hideRetryFab()");
        if (mIsShowingFab && mFloatingActionButton != null && mFloatingActionButton.getVisibility() == View.VISIBLE) {
            mIsShowingFab = false;
            startFabAnimation();
        }
    }

    private void startFabAnimation() {
        float value;
        if (mIsShowingFab) {
            value = -getResources().getDimension(R.dimen.fab_y_transition_value);
        } else {
            value = getResources().getDimension(R.dimen.fab_y_transition_value);
        }
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(
                mFloatingActionButton, "translationY", value);
        objectAnimator.setDuration(200);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (mIsShowingFab) {
                    mFloatingActionButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!mIsShowingFab) {
                    mFloatingActionButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        objectAnimator.start();
    }

    private void retryOnError() {
        mMainViewModel.retryOnError();
    }
}
