package com.sopherwang.myapplication;

/**
 * Created by Jiajun Wang on 1/11/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class AppConfig {
    public static final String LOG_PREFIX = "Prototype~";

    public static final String MESSAGE_DB_NAME = "message";
    public static final String MESSAGE_DB_TABLE_NAME = "messages";
    public static final String AUTHOR_DB_TABLE_NAME = "authors";

    public static final int LIMIT_NUMBER = 20;
    public static final int PREFETCH_DISTANCE = 8;
}
