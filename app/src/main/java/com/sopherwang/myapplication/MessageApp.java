package com.sopherwang.myapplication;

import android.app.Application;
import android.content.Context;

/**
 * Created by Jiajun Wang on 1/14/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MessageApp extends Application {
    private static MessageApp sInstance;
    private Context mContext;

    public MessageApp() {
        super();
        sInstance = this;
    }

    public static MessageApp getInstance() {
        return sInstance;
    }

    public static Context getContext() {
        return getInstance().mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }
}
