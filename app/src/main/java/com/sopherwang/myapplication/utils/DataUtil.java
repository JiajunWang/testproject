package com.sopherwang.myapplication.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jiajun Wang on 1/15/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class DataUtil {
    /**
     * Convert time strings. Eg, 2015-02-01T07:46:23Z to 07:46:23 2015-02-01
     *
     * @param timeString
     * @return convertedString
     */
    public static String convertTimeString(String timeString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
            Date date = dateFormat.parse(timeString);
            return newDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
