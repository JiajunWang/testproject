package com.sopherwang.myapplication.data.network.retrofit;

import com.sopherwang.myapplication.AppConfig;
import com.sopherwang.myapplication.data.models.ModelMessageList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jiajun Wang on 1/11/18.
 * Copyright (c) 2017. All rights reserved.
 */

public interface ApiStores {
    String BASE_URL = "http://message-list.appspot.com/";

    @GET("messages?limit=" + AppConfig.LIMIT_NUMBER)
    Observable<ModelMessageList> getMessageList();

    @GET("messages?limit=" + AppConfig.LIMIT_NUMBER)
    Observable<ModelMessageList> getMessageList(@Query("pageToken") String token);
}
