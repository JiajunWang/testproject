package com.sopherwang.myapplication.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.sopherwang.myapplication.data.models.ModelMessage;

/**
 * Created by Jiajun Wang on 1/13/18.
 * Copyright (c) 2017. All rights reserved.
 */

@Database(entities = {ModelMessage.class}, version = 1)
public abstract class MessageDatabase extends RoomDatabase {
    public abstract MessageDao messageDao();
}
