package com.sopherwang.myapplication.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jiajun Wang on 1/12/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class ModelMessageList extends ModelBase {
    @SerializedName("count")
    private int mCount;

    @SerializedName("pageToken")
    private String mPageToken;

    @SerializedName("messages")
    private List<ModelMessage> mMessageList;

    public int getCount() {
        return mCount;
    }

    public String getPageToken() {
        return mPageToken;
    }

    public List<ModelMessage> getMessageList() {
        return mMessageList;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public void setPageToken(String pageToken) {
        mPageToken = pageToken;
    }

    public void setMessageList(List<ModelMessage> messageList) {
        mMessageList = messageList;
    }
}
