package com.sopherwang.myapplication.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.sopherwang.myapplication.AppConfig;
import com.sopherwang.myapplication.data.models.ModelMessage;

import java.util.List;

/**
 * Created by Jiajun Wang on 1/13/18.
 * Copyright (c) 2017. All rights reserved.
 */
@Dao
public interface MessageDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public void insertMessages(List<ModelMessage> messageEntities);

    @Query("SELECT * FROM " + AppConfig.MESSAGE_DB_TABLE_NAME + " ORDER BY id ASC")
    public DataSource.Factory<Integer, ModelMessage> loadMessages();

    @Query("DELETE FROM " + AppConfig.MESSAGE_DB_TABLE_NAME + " WHERE id = :id")
    public void deleteMessage(long id);

    @Query("SELECT page_token FROM " + AppConfig.MESSAGE_DB_TABLE_NAME + " ORDER BY id DESC LIMIT 1")
    public String getNewestToken();
}
