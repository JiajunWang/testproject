package com.sopherwang.myapplication.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;

import com.google.gson.annotations.SerializedName;
import com.sopherwang.myapplication.AppConfig;

/**
 * Created by Jiajun Wang on 1/12/18.
 * Copyright (c) 2017. All rights reserved.
 */

@Entity(tableName = AppConfig.AUTHOR_DB_TABLE_NAME,
        indices = {@Index(value = {"name"}, unique = true)})
public class ModelAuthor extends ModelBase {
    @ColumnInfo(name = "name")
    @SerializedName("name")
    private String mName;

    @ColumnInfo(name = "photoUrl")
    @SerializedName("photoUrl")
    private String mPhotoUrl;

    public String getName() {
        return mName;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }
}
