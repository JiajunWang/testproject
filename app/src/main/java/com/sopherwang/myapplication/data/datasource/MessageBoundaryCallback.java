package com.sopherwang.myapplication.data.datasource;

import android.arch.paging.PagedList;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.sopherwang.myapplication.AppConfig;
import com.sopherwang.myapplication.AppExecutors;
import com.sopherwang.myapplication.data.local.MessageDatabase;
import com.sopherwang.myapplication.data.models.ModelMessage;
import com.sopherwang.myapplication.data.models.ModelMessageList;
import com.sopherwang.myapplication.data.network.retrofit.ApiStores;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jiajun Wang on 1/14/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MessageBoundaryCallback extends PagedList.BoundaryCallback<ModelMessage> {
    private static final String TAG = AppConfig.LOG_PREFIX + MessageBoundaryCallback.class.getSimpleName();
    private PagingRequestHelper mPagingRequestHelper;
    private ApiStores mApiStores;
    private MessageDatabase mMessageDatabase;

    public MessageBoundaryCallback(PagingRequestHelper pagingRequestHelper, ApiStores apiStores, MessageDatabase messageDatabase) {
        mPagingRequestHelper = pagingRequestHelper;
        mApiStores = apiStores;
        mMessageDatabase = messageDatabase;
    }

    @Override
    public void onZeroItemsLoaded() {
        Log.d(TAG, "onZeroItemsLoaded()");
        mPagingRequestHelper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL,
                helperCallback -> {
                    mApiStores.getMessageList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<ModelMessageList>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(ModelMessageList messageList) {
                                    Log.d(TAG, "onZeroItemsLoaded() - onNext()");
                                    handleResponse(messageList);
                                    helperCallback.recordSuccess();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d(TAG, "onZeroItemsLoaded() - onError(): " + e);
                                    helperCallback.recordFailure(e);
                                }

                                @Override
                                public void onComplete() {
                                    Log.d(TAG, "onZeroItemsLoaded() - onComplete()");
                                }
                            });
                });
    }

    @Override
    public void onItemAtEndLoaded(@NonNull ModelMessage itemAtEnd) {
        Log.d(TAG, "onItemAtEndLoaded(): itemAtEnd id is " + itemAtEnd.getId());
        mPagingRequestHelper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER,
                helperCallback -> {
                    String pageToken = itemAtEnd.getPageToken();
                    if (TextUtils.isEmpty(pageToken)) {
                        Log.e(TAG, "onItemAtEndLoaded() pageToken is empty");
                        helperCallback.recordFailure(new Throwable("pageToken is empty"));
                        return;
                    }

                    mApiStores.getMessageList(pageToken)
                            .subscribeOn(Schedulers.from(AppExecutors.getInstance().networkIO()))
                            .observeOn(Schedulers.from(AppExecutors.getInstance().mainThread()))
                            .subscribe(new Observer<ModelMessageList>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                }

                                @Override
                                public void onNext(ModelMessageList messageList) {
                                    Log.d(TAG, "onItemAtEndLoaded() - onNext()");
                                    handleResponse(messageList);
                                    helperCallback.recordSuccess();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d(TAG, "onItemAtEndLoaded() - onError(): " + e);
                                    helperCallback.recordFailure(e);
                                }

                                @Override
                                public void onComplete() {
                                    Log.d(TAG, "onItemAtEndLoaded() - onComplete()");
                                }
                            });
                });
    }

    @Override
    public void onItemAtFrontLoaded(@NonNull ModelMessage itemAtFront) {
        super.onItemAtFrontLoaded(itemAtFront);
        Log.d(TAG, "onItemAtFrontLoaded()");
    }

    private void handleResponse(ModelMessageList messageList) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            String pageToken = messageList.getPageToken();
            Log.d(TAG, "handleResponse(): pageToken is " + pageToken);
            List<ModelMessage> messages = messageList.getMessageList();
            for (ModelMessage modelMessage : messages) {
                modelMessage.setPageToken(pageToken);
            }
            mMessageDatabase.messageDao().insertMessages(messages);
        });
    }
}
