package com.sopherwang.myapplication.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.sopherwang.myapplication.AppConfig;

/**
 * Created by Jiajun Wang on 1/11/18.
 * Copyright (c) 2017. All rights reserved.
 */

@Entity(tableName = AppConfig.MESSAGE_DB_TABLE_NAME)
public class ModelMessage extends ModelBase {
    @ColumnInfo(name = "content")
    @SerializedName("content")
    private String mContent;

    @ColumnInfo(name = "updated")
    @SerializedName("updated")
    private String mUpdate;

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private long mId;

    @Embedded(prefix = "author")
    @SerializedName("author")
    private ModelAuthor mAuthor;

    @ColumnInfo(name = "page_token")
    private String mPageToken;

    public String getContent() {
        return mContent;
    }

    public String getUpdate() {
        return mUpdate;
    }

    public long getId() {
        return mId;
    }

    public ModelAuthor getAuthor() {
        return mAuthor;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setUpdate(String update) {
        mUpdate = update;
    }

    public void setId(long id) {
        mId = id;
    }

    public void setAuthor(ModelAuthor author) {
        mAuthor = author;
    }

    public String getPageToken() {
        return mPageToken;
    }

    public void setPageToken(String pageToken) {
        mPageToken = pageToken;
    }
}
