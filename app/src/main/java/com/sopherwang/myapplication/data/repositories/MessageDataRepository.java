package com.sopherwang.myapplication.data.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.arch.persistence.room.Room;
import android.util.Log;
import android.widget.Toast;

import com.sopherwang.myapplication.AppConfig;
import com.sopherwang.myapplication.AppExecutors;
import com.sopherwang.myapplication.MessageApp;
import com.sopherwang.myapplication.data.datasource.MessageBoundaryCallback;
import com.sopherwang.myapplication.data.datasource.PagingRequestHelper;
import com.sopherwang.myapplication.data.local.MessageDatabase;
import com.sopherwang.myapplication.data.models.ModelMessage;
import com.sopherwang.myapplication.data.network.retrofit.ApiStores;
import com.sopherwang.myapplication.data.network.retrofit.AppClient;

/**
 * Created by Jiajun Wang on 1/13/18.
 * Copyright (c) 2017. All rights reserved.
 */

public class MessageDataRepository {
    private static final String TAG = AppConfig.LOG_PREFIX + MessageDataRepository.class.getSimpleName();
    private static MessageDataRepository sInstance;

    private ApiStores mApiStores;
    private MessageDatabase mMessageDatabase;
    private PagingRequestHelper mPagingRequestHelper;
    private MutableLiveData<PagingRequestHelper.StatusReport> mStatusLiveData;

    private MessageDataRepository() {
        mApiStores = AppClient.retrofit().create(ApiStores.class);
        mMessageDatabase = Room.databaseBuilder(MessageApp.getContext(),
                MessageDatabase.class, AppConfig.MESSAGE_DB_NAME).build();
        mPagingRequestHelper = new PagingRequestHelper(AppExecutors.getInstance().pagingHelperThread());
        mStatusLiveData = new MutableLiveData<>();
        mPagingRequestHelper.addListener(status -> {
            /*if (status.hasRunning()) {
                mStatusLiveData.postValue(PagingRequestHelper.Status.RUNNING);
            } else if (status.hasError()) {
                // can also obtain the error via {@link StatusReport#getErrorFor(RequestType)}
                Log.e(TAG, "Response result " + status);
                mStatusLiveData.postValue(PagingRequestHelper.Status.FAILED);
                processingErrorsFromReport(status);
            } else {
                mStatusLiveData.postValue(PagingRequestHelper.Status.SUCCESS);
            }*/
            mStatusLiveData.postValue(status);
        });
    }

    public static MessageDataRepository getInstance() {
        if (sInstance == null) {
            sInstance = new MessageDataRepository();
        }
        return sInstance;
    }

    // https://issuetracker.google.com/issues/70573345
    // Current datasource
    public LiveData<PagedList<ModelMessage>> getMessageFromDb() {
        Log.d(TAG, "getMessageFromDb()");
        MessageBoundaryCallback messageBoundaryCallback = new MessageBoundaryCallback(mPagingRequestHelper, mApiStores, mMessageDatabase);
        PagedList.Config config = new PagedList.Config.Builder()
                .setPrefetchDistance(AppConfig.PREFETCH_DISTANCE)
                .setPageSize(AppConfig.LIMIT_NUMBER)
                .setEnablePlaceholders(true)
                .build();
        return new LivePagedListBuilder<>(mMessageDatabase.messageDao().loadMessages(), config)
                .setBoundaryCallback(messageBoundaryCallback)
                .build();
    }

    public LiveData<PagingRequestHelper.StatusReport> getPagingStatus() {
        return mStatusLiveData;
    }

    public void deleteMessage(long id) {
        Log.d(TAG, "deleteMessage() id = " + id);
        AppExecutors.getInstance().diskIO().execute(() -> {
            mMessageDatabase.messageDao().deleteMessage(id);
        });
    }

    public void retryOnError() {
        Log.d(TAG, "retryOnError()");
        mPagingRequestHelper.retryAllFailed();
    }
}
